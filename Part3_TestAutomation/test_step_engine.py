from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import logging
import time
import sys
import os


class TestStepEngine():
    """Class with all the selenium webdriver calls.
    The functions in this class are called with an "I" in front of it.
    This is to simulate a BDD style, e.g.: I click, I select from dropdown, etc.
    """

    SUCCESS = 'SUCCESS'
    FAILURE = 'FAILURE'
    WARNING = 'WARNING'

    web_driver = None

    # logging and screenshot settings
    time_string = time.strftime('%Y-%m-%d_%H%M%S')
    log_dir = os.path.abspath(os.path.dirname(__file__)) + '/log/'
    log_file_name = log_dir + time_string + '_testrun.log'
    screenshot_name = log_dir + FAILURE + '-screenshot_' + time_string + '.png'

    def __init__(self):
        """Initiator. If, for some reason (debugging for example) we include this class directly
        into a test case (not via the test_set_engine), this makes sure a default browser is set.
        """
        if self.web_driver is None:
            self.web_driver = webdriver.Firefox()

    def __log(self, status, value):
        """Write 'value' to the log and print it. Made into a separate function to prevent code duplication

        Args:
            status (str): SUCCESS or FAILURE. Decides if we need to log as info or error.
            value (str): value to log and print
        """
        if status == self.SUCCESS:
            logging.info(value)
        elif status == self.WARNING:
            logging.warning(value)
        else:
            logging.error(value)
        print(value)

    def __get_element(self, target, timeout=30):
        """Private function __get_element looks for 'timeout' seconds
        for a webdriver element defined by 'target' xpath or css selector.
        Writes a textual exception to a log file if element could not be found in time.

        Args:
            target (str): an xpath or css selector
            timeout (int): the amount of time we want to wait for element to be present

        Returns:
            element
        """
        try:
            web_driver_wait = WebDriverWait(self.web_driver, timeout)
            if target.find('/') == 0:
                return web_driver_wait.until(EC.presence_of_element_located((By.XPATH, target)))
            else:
                return web_driver_wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, target)))
        except:
            self.__log(self.WARNING, 'WARNING: Could not identify %s' % target)
            return None

    def go_to_url(self, target):
        """Tries to open a web url through selenium.
        Results are logged and printed.
        
        Args:
            target (str): the url to open
        """
        status = self.FAILURE

        try:
            self.web_driver.get(target)
            status = self.SUCCESS
        except:
            logging.info('EXCEPTION: Cannot open page %s' % target)
            sys.exit(0)

        self.__log(status, '%s: I try to open %s' % (status, target))

    def click(self, target):
        """Tries to click on a webdriver element.
        Results are logged and printed. Screenshot is taken on error.
        
        Args:
            target (str): xpath or css selector to element to click on
        """
        status = self.FAILURE

        try:
            element = self.__get_element(target)
            element.click()
            status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to click on %s' % (status, target))

    def fill_field(self, target, input):
        """Tries to insert a text into a webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            target (str): xpath or css selector to element to fill
            input (str): text to write
        """
        status = self.FAILURE

        try:
            element = self.__get_element(target)
            # select the value in the field to make sure we remove existing content
            element.send_keys(Keys.CONTROL, 'a')
            element.send_keys(input)
            status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to fill the field %s with %s' % (status, target, input))

    def select_from_dropdown(self, target, content, type='text'):
        """Tries to select an option from a webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            target (str): xpath or css selector to element to select
            content (str): the value or text to select
            type (str): either 'value' or 'text'
        """
        status = self.FAILURE

        try:
            select = Select(self.__get_element(target))
            if type == 'text':
                select.select_by_visible_text(content)
                status = self.SUCCESS
            elif type == 'value':
                select.select_by_value(content)
                status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to select %s from the dropdown %s' % (status, content, target))

    def wait_for_text(self, text, target = None, timeout=10):
        """Wait for max 'timeout' for 'text' to appear webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            text (str): text to look for
            target (str): xpath or css selector to element with text
            timeout (int): max amount of seconds we should wait
        """
        status = self.FAILURE

        try:
            if target is None:
                target = 'body'

            web_driver_wait = WebDriverWait(self.web_driver, timeout)
            if target.find('/') == 0:
                web_driver_wait.until(EC.text_to_be_present_in_element((By.XPATH, target), text))
            else:
                web_driver_wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, target), text))
            status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to wait for max %s seconds for text "%s"' % (status, timeout, text))

    def grab_text(self, target):
        """return text from webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            target (str): xpath or css selector to element with text
            text (str): text to look for
        Returns:
            str: grabbed text or None on failure
        """
        status = self.FAILURE
        text = None

        try:
            element = self.__get_element(target)
            if element is not None:
                text = element.text
            status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to grab text from element' % status)
        return text

    def can_see_text(self, text, target = None):
        """Validate if 'text' is present in webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            text (str): text to look for
            target (str): xpath or css selector to element with text. If None, search the entire html body
        """
        status = self.FAILURE

        try:
            if target is None:
                target = 'body'

            element = self.__get_element(target)
            grabbed_text = element.text
            if text in grabbed_text:
                status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try to see text "%s"' % (status, text))

    def cannot_see_text(self, text, target = None):
        """Validate if 'text' is NOT present in webdriver element.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            text (str): text to look for
            target (str): xpath or css selector to element with text. If None, search the entire html body
        """
        status = self.FAILURE

        try:
            if target is None:
                target = 'body'

            element = self.__get_element(target)
            grabbed_text = element.text
            if text not in grabbed_text:
                status = self.SUCCESS
        except:
            self.web_driver.save_screenshot(self.screenshot_name)

        self.__log(status, '%s: I try (not) to see text "%s"' % (status, text))

    def check_if_element_present(self, target):
        """Validate if webdriver element is present.
        Results are logged and printed. Screenshot is taken on error.

        Args:
            target (str): xpath or css selector to element with text

        Returns:
            bool
        """
        element = self.__get_element(target, 3)
        self.__log(self.SUCCESS, 'SUCCESS: I look for element "%s"' % target)
        if element is None:
            return False
        else:
            return True