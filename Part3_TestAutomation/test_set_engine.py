from test_step_engine import *
import logging


class TestSetEngine():
    """In this class you can:
    - Initiate a test set
    - Add abstracted test cases to the test test
    - Execute the test cases by calling test_step_engine
    """
    # set logging settings for this class and test_step_engine
    logging.basicConfig(
        filename=TestStepEngine.log_file_name,
        format='%(message)s',
        level=logging.INFO
    )

    def __init__(self, test_set_name):
        """Initiator.
        Sets the test set description and some variables used by multiple functions in this class

        Args:
            test_set_name (str): a description of the test set
        """
        self.test_set_name = test_set_name
        self.test_case_list = []
        self.test_case_index = 0


    def add_test_case(self, test_case):
        """Adds a test case from the test_cases folder to a list of to-be-executed test cases

        Args:
            test_case (obj): an object from the test_cases folder
        """
        self.test_case_list.append(test_case)


    def run_test_set(self, browsers=None):
        """Checks some settings and executes the list of test cases

        Args:
            browser (str): a browser name, used to decide which browser we are going to execute the tests in
        """
        if self.test_case_list != None:
            log_start = 'Testing test set: "%s"\r\n' % self.test_set_name
            logging.info(log_start)
            print(log_start)

            if browsers == None:
                browsers = 'Firefox'

            # make sure 'browsers' is a list (for multi-driver support)
            if not (isinstance(browsers, list)):
                browsers = [browsers]

            # execute all test cases for each driver.
            for browser in browsers:
                if browser.lower() == 'chrome':
                    TestStepEngine.web_driver = webdriver.Chrome()
                elif browser.lower() == 'ie':
                    TestStepEngine.web_driver = webdriver.Ie()
                else:
                    TestStepEngine.web_driver = webdriver.Firefox()
                    if browser.lower() != 'firefox':
                        browser = 'Firefox'
                        log_error = 'ERROR: %s not recognised.\r\n' % browser
                        logging.error(log_error)
                        print(log_error)

                browser = browser.capitalize()
                log_browser = 'Test execution started in %s. \r\n' % browser
                logging.info(log_browser)
                print(log_browser)

                # Loop through the test cases, executing them one by one
                while len(self.test_case_list) > (self.test_case_index):
                    current_test_case = self.test_case_list[self.test_case_index]

                    log_tc_start = 'Starting execution of test case %s in %s' % (current_test_case.__name__, browser)
                    logging.info(log_tc_start)
                    print(log_tc_start)

                    current_test_case(TestStepEngine())

                    log_tc_end = 'Finished executing test case %s in %s \r\n' % (current_test_case.__name__, browser)
                    logging.info(log_tc_end)
                    print(log_tc_end)

                    self.test_case_index += 1
                TestStepEngine.web_driver.quit()
                self.test_case_index = 0