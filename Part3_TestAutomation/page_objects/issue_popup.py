class IssuePopupPage():
    header = '//h2[@title="Create Issue"]'
    summary = '#summary'
    summary_error = summary + ' + div.error'
    submit_button = '#create-issue-submit'
    confirmation_field = '//div[contains(@class, "aui-message")]'
    confirmation_url = '//div[contains(@class, "aui-message")]/a'