from page_objects.home import *


class BoardPage():
    def url(rapid_view, project_key):
        """Return a board url based on rapid_view and project_key
        NOTE: I add quickFilter=119 (Only My Issues) to reduce loading time

        Args:
            rapid_view (int): view identifier
            project_key (str): project identifier

        Returns:
            string
        """
        base_path = '%ssecure/RapidBoard.jspa?rapidView=%d&projectKey=%s&quickFilter=119'
        return base_path % (HomePage.url, rapid_view, project_key)
