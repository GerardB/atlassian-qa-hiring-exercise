INSTALLATION

In order to run this test, you have to download and install the latest version of Python 3.
Next to the default 'pip' and 'setuptools', the interpreter also needs to install the 'selenium' package.
You can install these with just Python, but I advise to install the PyCharm IDE to make it easy.

Selenium uses the local browsers, so make sure you Firefox installed (and Chrome if you want to run on Chrome).
For browsers other than Firefox, Selenium requires an additional WebDriver, downloadable at  http://docs.seleniumhq.org/
Once downloaded, make sure the executables are located on a position known to your system PATH.

RUNNING THE TEST

In order to run the test, just execute the runner.py file with your Python interpreter (right click -> run in PyCharm).

ASSUMPTIONS

I assumed this exercise was to show my abilities with WebDriver and test automation, not so much with extensive
testing. That is why I only automated the happy flow and only part of the not-so-happy flow.

ISSUE FOUND

The TST project has so many issues created that the page loads for a very long time, sometimes even crashing the
browser tab. As a workaround I added quickFilter=119 to the url, which requests the page to only display the user's
issues.

A possible way to fix this issue might be to implement a lazy load: only request the data the user is currently seeing
and dynamically load the rest of the content when in comes in (or near) view.

DISCLAIMER

I am not a Python developer. I tried to follow the coding standards as much as possible, but it could be that I
missed some rules I am not aware of.

This Python-Selenium framework was put together in a hurry in the time span of 2 evenings and is far from perfect.
My goal with this framework was to develop a way of test automation that can be easily read with the "I do this"
notation. It is definitely lacking some exception handling and other technical finesse. I hope that isn't a problem.