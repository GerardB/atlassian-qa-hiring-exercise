from page_objects.login import *
from page_objects.account import *
from data_objects.account import *
from data_objects.login import *

class Login():
    def __init__(self, I, email, password):
        I.go_to_url(LoginPage.url)
        if I.check_if_element_present(LoginPage.submit):
            I.click(LoginPage.submit)
            I.can_see_text(LoginData.email_error, LoginPage.email_error)
            I.can_see_text(LoginData.password_error, LoginPage.password_error)
            I.fill_field(LoginPage.email, email)
            I.fill_field(LoginPage.password, password)
            I.click(LoginPage.submit)
        I.wait_for_text(AccountData.header, AccountPage.header)
