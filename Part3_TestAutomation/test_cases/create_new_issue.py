from test_cases.step_objects.login import *

from page_objects.board import *
from page_objects.header import *
from page_objects.issue_popup import *
from page_objects.issue import *

from data_objects.issue_popup import *
from data_objects.login import *
from data_objects.issue import *

class CreateNewIssue():
    def __init__(self, I):
        Login(I, LoginData.email, LoginData.password)

        I.go_to_url(BoardPage.url(46, 'TST'))
        I.click(Header.create_button)
        I.wait_for_text(IssuePopupData.header, IssuePopupPage.header)
        I.click(IssuePopupPage.submit_button)
        I.can_see_text(IssuePopupData.summary_error, IssuePopupPage.summary_error)
        I.fill_field(IssuePopupPage.summary, 'This is a summary')
        I.click(IssuePopupPage.submit_button)

        I.wait_for_text(IssuePopupData.confirmation_text, timeout=20)
        I.click(IssuePopupPage.confirmation_url)
        I.wait_for_text('This is a summary', IssuePage.description)
        I.cannot_see_text(IssueData.no_issue_error)