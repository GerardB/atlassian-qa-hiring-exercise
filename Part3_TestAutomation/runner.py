from test_set_engine import *
from test_cases.create_new_issue import *

if __name__ == '__main__':

    # define a test set
    test_set = TestSetEngine('Test if it is still possible to create an issue.')

    # you can add more than one test case, but at the moment we only have one
    test_set.add_test_case(CreateNewIssue)

    # run the test set in 'chrome' or 'firefox' or both ['chrome', 'firefox']
    test_set.run_test_set('firefox')

